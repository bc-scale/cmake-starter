<div align="center">
  <img width="1000" height="400" src="assets/cmake-starter.jpg">
</div>
<div align="center">
  <img src="https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square">
  <br>
  <p>A <b>cmake starter</b> template for modern C++ development</p>
</div>

## Table of Contents

- [Background](#background)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Background

    A lightweight Cmake project that uses CPM as its package manager. There are many
    CMake starter kits out there, this one focuses on a Linux environment only, there is no
    support for Windows or MacOs built in. The entire system assumes a Linux Environment.

1. [CPM -- The missing package manager for CMake](https://github.com/cpm-cmake/CPM.cmake)

- CPM was chosen because it is simple to set up, dependency free, and is a simple wrapper around
  Cmakes FetchContent. Most other package managers like Hunter or Conan are difficult to set up,
  make ci/cd pipelines even more difficult to set up than they already are, and have dependencies.

2. This templated project has 2 types of projects ready to go by default.

- The `core` folder showcases how to set up a shared library in CMake
- The `app` folder showcases a standard binary application which consumes the `core` shared library
- If you just want to build a library, delete the `app` folder, and purge the CMakeLists.txt files of `app` specific code
- IF you just want to build a regular ol' binary application, delete the `core` folder, and purge the CMakeLists.txt files or `core` specific code

## Usage

> Standard cmake rules apply, the following commands should be made from the root of the project

1. `cmake -B build` Generate the make files, fetch build dependencies
2. `cmake --build build -j$(nproc)` Compile the code

### The `devkit` tool

`devkit` is a collection of python modules that perform common tasks
for developers and package maintainers. It has **zero** dependencies.
Just the vanilla python standard library. Python version >= 3.6 should
be fine, the tool is wrapped by a Makefile.

```bash
# Run for all options
make list

# format all projects with clang-format
make format

# statically analyze all files
make lint

# There is also a small helper utility for rapid compiling while developing,
# Note that the compile command does not modify CMake options, but only changes
# the `CMAKE_CXX_COMPILER` and `CMAKE_C_COMPILER` option

# compile with the clang toolchain
make build_clang

# compile with the gnu toolchain
make build_gnu
```

### Modifying `devkit` for your project

The clang toolchain and gnu toolchain can have different names
on different distros depending on how you choose to install it,
for example maybe you want to install and use the latest version of `clang-format`,
the program maybe called `clang-format-13`, for this reason `devkit` as a settings file
used for configuration.

> To modify the cmake toolchain options, open the `tooling/settings.py` file
> and edit the `CMAKE_PROGRAMS` dictionary.

```python
CMAKE_PROGRAMS: Dict[str, Dict[str, str]] = {
    # cmake is here incase a different version is being used,
    # using flags should not be necessary
    "CMAKE": {
        "name": "cmake",
        "flags": "",
    },
    # customize formatting and clang tidy through the flags
    "CLANG_FORMATTER": {
        "name": "clang-format",
        "flags": "-i"
    },
    "CMAKE_FORMATTER": {
        "name": "cmake-format",
        "flags": "-i"
    },
    "CLANG_ANALYZER": {
        "name": "clang-tidy",
        "flags": f"-p {PROJECT_BUILD_DIR} --config-file={PROJECT_ROOT / '.clang-tidy'}"
    },
    # modify your compiler versions here, flags should not be necessary
    "CLANG_CXX_COMPILER": {
        "name": "clang++",
        "flags": "",
    },
    "CLANG_C_COMPILER": {
        "name": "clang",
        "flags": "",
    },
    "GNU_CXX_COMPILER": {
        "name": "g++",
        "flags": "",
    },
    "GNU_C_COMPILER": {
        "name": "gcc",
        "flags": "",
    }
}
```

> To modify the packaging options, open the `tooling/settings.py` file
> and edit the `PACKAGE_CONFIG` dictionary.

```python
PACKAGE_CONFIG: Dict[str, str] = {
    "BACKEND": "checkinstall",
    "LICENSE": "GPL-3.0",
    "MAINTAINER": "mattcoding4days",
    "REQUIRES": "",
    "RELEASE": f"{distro.codename()}-{distro.version()}"
}
```

> If there are files you don't want to format, or that you don't want to
> analyze add them to ignore lists. open the `tooling/settings.py`
> and edit the following

```python
# Files that should be ignored for formatting (clang-format, cmake-format)
FORMAT_IGNORE: List[Path] = [
    PROJECT_ROOT / 'app' / 'src' / 'example_file.cpp',
    PROJECT_ROOT / 'app' / 'src' / 'some_other_file.cpp'
]

# Files that should be ignored for clang-tidy
ANALYZE_IGNORE: List[Path] = [
    PROJECT_ROOT / 'app' / 'src' / 'example_file.cpp',
    PROJECT_ROOT / 'app' / 'src' / 'some_other_file.cpp'
]
```

### Docker

1. Build.Dockerfile builds and runs all tests in the project, simulates a ci/cd pipeline
2. Remote-Dev.Dockerfile is for remote development through Clion or vscode

> This image is for running tests, simulates what would happen in a ci/cd pipeline

```bash
# Build the container (can be used to rebuild image after code changes)
# [e.g] docker image build -t <image-name>:<tag> -f dev_containers/<file>.Dockerfile .
docker image build -t starter:v0.1 -f dev_containers/Build.Dockerfile .

# Rebuild with no cache
docker image build --no-cache -t starter:v0.1 -f dev_containers/Build.Dockerfile.

# Run the container interactively
# [e.g] docker container run -it <image-name>:<tag>
docker container run -it starter:v0.1

# Run non interactively
docker container run starter:v0.1
```

## Maintainers

[@thebashpotato](https://gitlab.com/thebashpotato)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2022 Matt Williams
