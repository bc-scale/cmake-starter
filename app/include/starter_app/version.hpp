#ifndef STARTER_APP_VERSION_HPP
#define STARTER_APP_VERSION_HPP

/**
 * @file starter_app/version.hpp
 * @author Matt Williams (matt.k.williams@protonmail.com)
 * @brief Adds version support for project, used by Cmake
 * @date 2021-11-19
 */

#include <string>

namespace starter::app {
constexpr auto VMAJOR = "0";
constexpr auto VMINOR = "1";
constexpr auto VPATCH = "0";
/**
 * @brief return the version in concatenated format
 *
 * @returns std::string
 * */
[[maybe_unused]] std::string get_version();
} // namespace starter::app

#endif
