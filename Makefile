SHELL := $(shell command -v sh)

define _list
	cat << EOF
		Available commands
		==================
		list
		docs
		build_gnu
		build_clang
		format
		lint
		clean
	EOF
endef

define _docs
	printf "Building docs\n"
	[ -d ./doxygen/html ] && rm -rf ./doxygen/html
	doxygen ./doxygen/Doxyfile
endef

define _clean
	printf "Cleaning build artifacts\n"
	[ -d build ] && rm -rf build
	[ -d build-debug ] && rm -rf build-debug
	[ -d build-debug-remote ] && rm -rf build-debug-remote
	[ -d cmake-build-debug ] && rm -rf cmake-build-debug
	[ -d cmake-build-debug-remote ] && rm -rf cmake-build-debug-remote
	printf "\n"
endef

list:
	@$(call _list)

docs:
	@$(call _docs)

build_gnu:
	@python3 scripts/devkit.py compile --gnu

build_clang:
	@python3 scripts/devkit.py compile --clang

format:
	@python3 scripts/devkit.py clang --format

lint:
	@python3 scripts/devkit.py clang --lint

clean:
	@$(call _clean)

.ONESHELL:
.Phony: list build_gnu build_clang format lint clean
